#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  generate_tables.py
#
#  Copyright 2014 Alfredo J. Mejia <mejia@cida.ve>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
"""This script generate random observations with camera  schmidt + hartmann
screen configuration. The simulation is focused in sources  positions in CCDs,
useful for  calibration the focus computation methods. The name(s) of the
file(s) written is(are) consistent with the CCD nomenclature used for the
Schmidt Camera.

Usage
-----

  generate_tables [options] n_sources distance angle orphans_percent dir

  n_sources : int scalar
      Number of sources. Note please that this is not necessarily the final
      number of sources appearing  in each CCD because of the  boundary effects.
      Also if orphans_percent is greater than 0 a fraction of n_sources will be
      simulated as orphans sources (see orphans_percent for details).
  distance : float scalar
      Distance sepparating the members of the pairs. This value is then
      perturbed inside the camera  object in order to take into account
      inaccuracies introduced in the source detection process.
  angle : float scalar
      Angle in degrees between the line joining the members of a pair and the
      horizontal.
  orphans_percent : float scalar
      Percentual fraction of n_sources wanted to be orphans. This is computed
      in such a way that:
        n_orphans + 2 * n_pairs + n_losses = n_sources,
      where n_losses is the number of sources lying outside the CCD.
  dir : string
      Path where to save the simulated observations.

Options
-------

  -h --help : display this message on screen.
"""
import numpy as np
import sys, getopt
from itertools import product
from mock_images import MockHartmannImages

try:
    optlist, args = getopt.getopt(sys.argv[1:], "h", ["help"])
except getopt.error, msg:
    print("ERROR: {0}\n".format(msg))
    print(__doc__)
    sys.exit(1)
else:
    for o, v in optlist:
        if o == "-h" or o == "--help":
            print(__doc__)
            sys.exit(0)
        else:
            print("\nERROR: invalid option.")
            print("USAGE: [options] generate_tables n_sources distance angle\
                    orphans_percent dir")
            print("\nUse -h option for more details.\n")
            sys.exit(1)

if len(args) < 5 or len(args) > 5:
    raise ValueError, "There are some missing/extra arguments. Use -h option."

n_sources, distance, angle, orphans_percent, dir = args
ccd_shape = (2048, 2048)
cam_ccd_mask = np.ones((4, 4), dtype = np.bool)

camera = MockHartmannImages(eval(n_sources), ccd_shape, cam_ccd_mask,
                    eval(distance), eval(angle), eval(orphans_percent))

tables = camera.get_ccd_images()

cam_props = camera.get_camera_props()
dat_props = camera.get_image_props()

print "-------------------------------------------------------------------"
print "Camera properties:"
for prop in sorted(cam_props):
    print "{0:25s} = {1}".format(prop, cam_props[prop])
print "-------------------------------------------------------------------"
print "Data properties:"
for prop in sorted(dat_props):
    print "{0:25s} = {1}".format(prop, dat_props[prop])
print
for i, j in product(xrange(cam_ccd_mask.shape[0]),
                    xrange(cam_ccd_mask.shape[1])):
    np.savetxt("{0}/qnx{1}_ccd{2}".format(dir, i, j), tables[i][j])

camera.show_image_array()
