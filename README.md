# README #

This repository contains a code intended to compute the focus of a telescope, given a set of images of the sky taken with a Hartmann screen. This document will guide you to get that code running.

### What is this repository for? ###

The FocusHartmann repository comprises a set of programs for testing/running the main application dubbed `Focus_app.py` that computes the focus of a telescope given two arrays of images taken with a Hartmann screen. The main features of this program are:

* A fully functional GUI.
* The user has wide control over the main parameters of the focus computation.
* Robust estimates of the telescope's focus with error bars right out of the box.

### How do I get set up? ###

In order to get `Focus_app.py` running you'll have to install some dependencies first, namely:

* python
* python-numpy
* python-matplotlib
* python-qt4

Paste the following line in a (debian-based) linux terminal to get this done:

```
#!bash
sudo apt-get install python python-numpy python-matplotlib python-qt4

```

Now you are ready to run, code and test the several programs in this repository.

#### A testing program: `generate_tables.py` ####

In order to test the `Focus_app.py` application, we developed a program that generates a set of tables with (x,y) positions in a CCD array: `generate_tables.py`. By default this program simulates an array of 4x4 images with duplicated sources (as in Hartmann screen images), given a set of inputs, namely:

* Number of sources per CCD,
* Distance between the source pairs in pixels,
* Angle of the line joining the pairs respect to the horizontal,
* Percent of widow sources (sources without a pair) to emulate CCD flaws.

This program has no GUI (yet). Additionally, `generate_tables.py` will randomly perturb the (x,y) values of all the sources to emulate CCD flaws, errors in the position determination produced by the source extractor itself and atmospheric effects, like the seeing.

#### The main program: `Focus_app.py` ####

`Focus_app.py` will compute a statistically robust estimate of the focus of a telescope, provided two arrays with the position of the sources extracted from Hartmann images and the focus values with which those images were taken. When the user `Draw` the sources on the array area (see image below), the program will wait for the user to introduce the pattern definition. This is done by clicking on two (and only two) sources the user thinks define the pattern. The user can `Reset` any selection any moment before starting the computation of the distances between pair members (i.e., before clicking `Go!`).

When `Focus_app.py` is executed, the following windows will be launch:

![FH_app_init.png](https://bitbucket.org/repo/ARqedL/images/3650751063-FH_app_init.png)

Where every marked aspect has a function:

1. The path to the ASCII tables with the sources positions is introduced here either, via the file manager pop-up or by typing in a suitable path.
2. There are three buttons: `Draw` which will display the sources in the array area (3); `Reset`, that will erase any pattern definition made; and `Go!`, which indicates when to start computing the distance given the pattern.
3. The array area where the sources will be drawn and the patter will be defined by the user.
4. The focus value with which the current image was taken.
5. Some sort of instructions will be displayed here through the whole process.
6. The main results are summarized in this area, along with the focus computed.


### Contribution guidelines ###

There are several task you can do that would contribute to this project.

#### Coding ####

If you feel confident enough to write some python lines, you're free to [fork](https://bitbucket.org/ajmejia/focushartmann/fork) this repository. But before you write any line, please make sure you'll follow this [coding guidelines](http://legacy.python.org/dev/peps/pep-0008/), to have a self-consistent and maintainable code.

#### Testing ####

Even if you don't want to write any code line, you can still help on testing what is done and give us your feedback via our [Issue Tracker](https://bitbucket.org/ajmejia/focushartmann/issues). You can file either bugs or feature request and we'll try to translate that into a fully functional experience to the user.

To get all you need for experiment and test `Focus_app.py`, please download our code [here](https://bitbucket.org/ajmejia/focushartmann/downloads).

#### Designing ####

Design is not all about beauty, it's about functionality too. Intended to provide the most confortable user experience, we want to take care of every aspect of the GUI design. You can contribute to improve the user interface by providing some mock-ups, like this one:

![FH_mockup.png](https://bitbucket.org/repo/ARqedL/images/1776565673-FH_mockup.png)

We strongly recommend using mock-ups constructors with GUI support like [Pencil](http://pencil.evolus.vn/). Also, if you feel brave enough to convert your mock-up into a real windows, we recommend you to use the [qt4-designer](http://qt-project.org/doc/qt-5/designer-to-know.html) and `pyuic4` or `pyside-uic` applications to convert the .ui file produced by the qt-designer into a python usable module.

In a debian-based system, this applications are installed by typing in a terminal:


```
#!bash

sudo apt-get install qt4-designer pyqt4-dev-tools
```

If you want to use `pyside-uic`, then substitute `pyqt4-dev-tools` above by `pyside-tools`.

#### Documenting ####

Writing documentation for a software is a very important task, but more important is to have that documentation to date with the software changes. So, if you want to contribute on this, please check out to our [Wiki](https://bitbucket.org/ajmejia/focushartmann/wiki/browse/) and start with some [Markdown](http://daringfireball.net/projects/markdown/) lines.

### Any doubt? ###

Please, don't hesitate in contact me via e-mail: mejia@cida.ve.