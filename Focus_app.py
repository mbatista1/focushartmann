import numpy as np
import sys
from PyQt4 import QtCore
from PyQt4 import QtGui
from focus_mw import Ui_MainWindow
from string import replace

DELTA_X, DELTA_Y = 20., 20.

class FocusSchmidtMW(QtGui.QMainWindow, Ui_MainWindow):

    def __init__(self, parent = None):
        super(FocusSchmidtMW, self).__init__(parent)

        self.setupUi(self)

        self.first_image = True
        self.base_msg    = str(self.msg.text()) + " "
        self.msg.setText(self.base_msg +
        "select a directory path from File menu and click Draw Array button")

        QtCore.QObject.connect(self.actionOpen, QtCore.SIGNAL("triggered()"),
            self.select_directory_path)
        QtCore.QObject.connect(self.pushButton, QtCore.SIGNAL("clicked()"),
            self.draw_ccd_array)
        QtCore.QObject.connect(self.pushButton_3, QtCore.SIGNAL("clicked()"),
            self.redraw_ccd_array)
        QtCore.QObject.connect(self.pushButton_2, QtCore.SIGNAL("clicked()"),
            self.focus_finder)
        QtCore.QObject.connect(self.actionClose, QtCore.SIGNAL("triggered()"),
            QtGui.qApp, QtCore.SLOT("quit()"))
        #QtCore.QObject.connect(self.actionDocumentation,
            #QtCore.SIGNAL("triggered()"), self.show_user_manual)
        #QtCore.QObject.connect(self.actionAbout,
            #QtCore.SIGNAL("triggered()"), self.show_app_info)

    def __choice_point(self, array):
        ind = np.random.randint(array.shape[0])
        return ind

    def __distance(self, point0, point1):
        deltas = point1 - point0
        return list(np.sqrt(np.sum(deltas ** 2, axis = 1)))

    def __pattern_mask(self, point, r, theta, table):
        x0, y0 = point
        xs, ys = table[:, 0], table[:, 1]

        x1 = np.array([r * np.cos(theta) + x0, r * np.cos(theta + np.pi) + x0])
        y1 = np.array([r * np.sin(theta) + y0, r * np.sin(theta + np.pi) + y0])

        xmask = ((xs >= x1[0] - DELTA_X / 2) & (xs <= x1[0] + DELTA_X / 2)) |\
                ((xs >= x1[1] - DELTA_X / 2) & (xs <= x1[1] + DELTA_X / 2))
        ymask = ((ys >= y1[0] - DELTA_Y / 2) & (ys <= y1[0] + DELTA_Y / 2)) |\
                ((ys >= y1[1] - DELTA_Y / 2) & (ys <= y1[1] + DELTA_Y / 2))

        return xmask, ymask

    def __focus(self, f1, f2, d1, d2):
        return (d1*f2 + d2*f1)/(d1 + d2)

    def select_directory_path(self):
        path = QtGui.QFileDialog.getExistingDirectory()
        if path:
            self.lineEdit.setText(path)
            self.pushButton.setEnabled(True)

    def get_tables_inpath(self, path):
        import os
        if self.first_image:
            file_path = (os.path.join(root, file) for root, subdirs, files in
                os.walk(path) for file in files if "hartmann1" in root)
        else:
            file_path = (os.path.join(root, file) for root, subdirs, files in
                os.walk(path) for file in files if "hartmann2" in root)

        return [np.loadtxt(file_path) for file_path in file_path]

    def draw_ccd_array(self):
        self.msg.setText(self.base_msg +
        "define search pattern by clicking two sources and click Go! button")
        self.widget.canvas.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.pushButton.setEnabled(False)
        path = str(self.lineEdit.text())
        self.tables = self.get_tables_inpath(path)

        for i, table in enumerate(self.tables):
            self.widget.canvas.axes[i].plot(table[:, 0], table[:, 1], "k.",
                picker = 4)

        self.widget.canvas.draw()

    def redraw_ccd_array(self):
        self.pushButton_2.setEnabled(False)
        self.pushButton_3.setEnabled(False)
        i  = self.widget.canvas.axes.index(self.widget.canvas._last_axes)

        xl = self.widget.canvas.axes[i].get_xlim()
        yl = self.widget.canvas.axes[i].get_ylim()
        self.widget.canvas.axes[i].clear()
        self.widget.canvas.axes[i].plot(self.tables[i][:, 0],
            self.tables[i][:, 1], ".k", picker = 4)
        self.widget.canvas.axes[i].redraw_in_frame()
        self.widget.canvas.axes[i].set_xlim(xl)
        self.widget.canvas.axes[i].set_ylim(yl)
        self.widget.canvas.draw()

        self.widget.canvas._last_axes        = None
        self.widget.canvas._PickEvent_counts = 0

    def focus_finder(self):
        self.msg.setText(self.base_msg)
        #self.menuBar.setEnabled(False)
        self.lineEdit.setEnabled(False)
        self.pushButton.setEnabled(False)
        self.pushButton_2.setEnabled(False)
        self.pushButton_3.setEnabled(False)
        self.widget.canvas.setCursor(QtGui.QCursor(QtCore.Qt.ForbiddenCursor))

        self.widget.canvas.disconnect_events()

        self.distances = []
        self.r, self.theta = self.widget.canvas.pattern
        for i, table in enumerate(self.tables):
            shape = table.shape

            while table.shape[0] > 1:
                j = self.__choice_point(table)

                point = table[j]

                xm, ym = self.__pattern_mask(point, self.r, self.theta, table)
                d = self.__distance(point, table[(xm) & (ym), :])
                self.distances += list(d)
                table = np.delete(table, j, axis = 0)

            xl = self.widget.canvas.axes[i].get_xlim()
            yl = self.widget.canvas.axes[i].get_ylim()
            self.widget.canvas.axes[i].clear()
            self.widget.canvas.axes[i].set_xlim(xl)
            self.widget.canvas.axes[i].set_ylim(yl)

        if self.first_image:
            self.d1 = np.mean(self.distances)

            self.i1_dist_p.setText(replace(str(self.i1_dist_p.text()),
                "--", "%.1f" % self.r))
            self.i1_angl_p.setText(replace(str(self.i1_angl_p.text()),
                "--", "%.1f" % (self.theta / np.pi * 180.0)))
            self.i1_npairs.setText(replace(str(self.i1_npairs.text()),
                "--", "%d"   % len(self.distances)))
            self.i1_mean_dist.setText(replace(str(self.i1_mean_dist.text()),
                "--", "%.1f" % self.d1))
            self.i1_std_dist.setText(replace(str(self.i1_std_dist.text()),
                "--", "%.1f" % np.std(self.distances)))
            self.frame.update()

            self.first_image = False
            self.widget.canvas.connect_events()
            self.widget.canvas._last_axes        = None
            self.widget.canvas._PickEvent_counts = 0
        else:
            self.d2 = np.mean(self.distances)
            #f = self.__focus(self.f1, self.f2, self.d1, self.d2)

            self.i2_dist_p.setText(replace(str(self.i2_dist_p.text()),
                "--", "%.1f" % self.r))
            self.i2_angl_p.setText(replace(str(self.i2_angl_p.text()),
                "--", "%.1f" % (self.theta / np.pi * 180.0)))
            self.i2_npairs.setText(replace(str(self.i2_npairs.text()),
                "--", "%d"   % len(self.distances)))
            self.i2_mean_dist.setText(replace(str(self.i2_mean_dist.text()),
                "--", "%.1f" % self.d2))
            self.i2_std_dist.setText(replace(str(self.i2_std_dist.text()),
                "--", "%.1f" % np.std(self.distances)))
            #self.focus.setText(replace(str(self.f), "--", "%.1f" % f))
            self.frame.update()

            return

        self.draw_ccd_array()

app = QtGui.QApplication(sys.argv)

fmw = FocusSchmidtMW()
fmw.show()

sys.exit(app.exec_())
