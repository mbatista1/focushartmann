#  camera.py
#
#  Copyright 2014 Alfredo J. Mejia <mejia@cida.ve>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
import matplotlib

matplotlib.rcdefaults()

from PyQt4 import QtGui, QtCore
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import numpy as np
import matplotlib.pyplot as plt

class CCDArray(FigureCanvas):
    """Define and connect events on CCD array."""

    def __init__(self, CCD_shape, array_shape):
        if np.isscalar(CCD_shape):
            self.npix_x = CCD_shape
            self.npix_y = CCD_shape
        elif len(CCDArray) == 2:
            self.npix_x = CCD_shape[0]
            self.npix_y = CCD_shape[1]
        else:
            raise ValueError, "`CCD_shape` must be scalar integer or iterable\
                                of two integers."

        if np.isscalar(array_shape):
            self.array_shape = (array_shape,) * 2
        elif len(array_shape) == 2:
            self.array_shape = array_shape
        else:
            raise ValueError, "`array_shape` must be scalar integer or iterable\
                                of two integers."

        self.fig = Figure(dpi=90)
        self.fig.subplots_adjust(wspace=0.05, hspace=0.05)

        self._PickEvent_counts = 0
        self._last_CCD = None

        self.CCDs = []
        for i in xrange(np.prod(self.array_shape)):
            self.CCDs.append(self.fig.add_subplot(4, 4, 1+i))

            self.CCDs[-1].set_xlim(1, 2048)
            self.CCDs[-1].set_ylim(1, 2048)
            self.CCDs[-1].tick_params(labelbottom=False, labelleft=False)

        FigureCanvas.__init__(self, self.fig)
        FigureCanvas.setSizePolicy(self, QtGui.QSizePolicy.Expanding,
            QtGui.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

        self.fig.tight_layout()

        self.connect_events()

    def _connect_PickEvent(self):
        self._PickEvent_id = self.fig.canvas.mpl_connect("pick_event",
            self._get_pattern)

    def _connect_LocationEvent(self):
        self._LocationEvent_id1 = self.fig.canvas.mpl_connect(
            "axes_enter_event", self._get_current_CCD)

    def connect_events(self):
        self._connect_LocationEvent()
        self._connect_PickEvent()

    def disconnect_events(self):
        self.figure.canvas.mpl_disconnect(self._LocationEvent_id1)
        self.figure.canvas.mpl_disconnect(self._LocationEvent_id2)
        self.figure.canvas.mpl_disconnect(self._PickEvent_id)

    def _get_pattern(self, point0, point1):
        r = np.sqrt((point1[0] - point0[0])**2 + (point1[1] - point0[1])**2)
        theta = np.arctan((point1[1] - point0[1])/(point1[0] - point0[0]))
        return r, theta

    def _get_CCD_sources(self, which_CCD):
        self.line = which_CCD.get_lines()[0]
        return self.line.get_xydata()

    def _del_CCD_sources(self, which_CCD, which_sources):
        old_data = self._get_CCD_sources(which_CCD)
        new_data = np.delete(old_data, which_sources, axis=0)

        x, y = old_data[which_sources][0]
        which_CCD.plot(x, y, ".r")

        self.line.set_xdata(new_data[:, 0])
        self.line.set_ydata(new_data[:, 1])
        self.figure.canvas.draw()

        return x, y

    def _get_current_CCD(self, event):
        self.setCursor(QtGui.QCursor(QtCore.Qt.CrossCursor))
        self._CCD = event.inaxes

    def _get_pattern(self, event):
        if self._last_CCD is None:
            self._last_CCD = self._CCD
        elif self._last_CCD is not self._CCD:
            return

        self._PickEvent_counts += 1
        if self._PickEvent_counts == 1:
            self.parent().parent().parent().pushButton_3.setEnabled(True)
            self._isource = event.ind
            self.point0 = self._del_CCD_sources(self._last_CCD, self._isource)
        elif self._PickEvent_counts == 2:
            self.parent().parent().parent().pushButton_2.setEnabled(True)
            self._isource = event.ind
            self.point1 = self._del_CCD_sources(self._last_CCD, self._isource)

            self.pattern = self._get_pattern(self.point0, self.point1)

class Camera(QtGui.QWidget):

    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)

        self.canvas = CCDArray(2048, 4)

        self.gl = QtGui.QGridLayout()
        self.gl.addWidget(self.canvas)
        self.setLayout(self.gl)
