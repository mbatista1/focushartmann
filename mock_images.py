#  mock_images.py
#
#  Copyright 2014 Alfredo J. Mejia <mejia@cida.ve>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
import matplotlib.pyplot as plt
import numpy as np
from itertools import product

class MockHartmannImages:

    '''Generate synthetic CCD images as taken with a Hartmann screen.

    MockHartmannImages is a class to randomly generate CCD images as taken with
    a Hartmann screen, with realistic error sources, such as read out noise, sky
    background, etc.

    Parameters
    ----------
    n_sources : int scalar
        Number of sources (per CCD) to simulate. Note that this is not
        necessarily the final number of sources in the (x, y) table  generated,
        considering that all sources must be in the square defined by (1, 1) and
        (n_pixels, n_pixels).
    CCD_shape : int scalar, int tuple
        Number of pixels  of the CCD where the sources will be simulated (it is
        considered) to be a square.
    CCD_array_mask : bool array
        A mask for the CCD array. When True CCD is present, absent otherwise.
    distance : int scalar, int tuple
        A tuple containing the range of possible distances between members of
        each pair of sources. Defaults to (100, 500). If is scalar, the value is
        the assumed distance.
    angle : float, optional
        A value for the angle between the pair and the horizontal.
    widows : float, optional
        A percentual value indicating the fraction of the sources being widows.
        Defaults to 0.
    noisy : bool, optional
        True to introduce noise in data; False: data is unperturbed. Defaults
        to True.
    '''

    def __init__(self, n_sources, CCD_shape, CCD_array_mask, distance=(100,
                    500), angle=None, widows_percent=0, noisy=True):
        self.n_sources = n_sources
        self.CCD_array_mask = CCD_array_mask
        self.CCD_array_shape = CCD_array_mask.shape

        if np.isscalar(CCD_shape):
            self.npix_x = self.npix_y = CCD_shape
        elif len(CCD_shape) == 2:
            self.npix_x, self.npix_y = CCD_shape
        else:
            raise ValueError, "`CCD_shape` must be scalar integer or iterable\
                                of two integers."

        if np.isscalar(distance):
            self.distance = distance
        elif len(distance) == 2:
            self.distance = np.random.random_integers(distance[0], distance[1])
        else:
            raise ValueError, "`distance` must be a scalar or a iterable of\
                                length two."

        if angle is not None:
            self.angle = angle
        else:
            self.angle = 2 * 180 * np.random.rand()

        n_widows = int(widows_percent / 100.0 * self.n_sources)
        n_pairs = int((self.n_sources - n_widows) * 0.5)

        self.actual_nsources = 0
        self.camera = [[None for j in xrange(self.CCD_array_shape[1])]\
                        for i in xrange(self.CCD_array_shape[0])]

        for i, j in product(xrange(self.CCD_array_shape[0]),
                            xrange(self.CCD_array_shape[1])):
            if not self.CCD_array_mask[i, j]:
                continue

            x = np.random.random_integers(1, self.npix_x, n_pairs)
            y = np.random.random_integers(1, self.npix_y, n_pairs)

            sign = np.random.choice([-1, +1], n_pairs)
            x_d  = x + sign * self.distance * np.cos(self.angle * np.pi / 180)
            sign = np.random.choice([-1, +1], n_pairs)
            y_d  = y + sign * self.distance * np.sin(self.angle * np.pi / 180)

            x_o = np.random.random_integers(1, self.npix_x, n_widows)
            y_o = np.random.random_integers(1, self.npix_y, n_widows)

            if noisy:
                pix_dev = 5
                x += np.random.random_integers(- pix_dev, pix_dev, n_pairs)
                y += np.random.random_integers(- pix_dev, pix_dev, n_pairs)
                x_d += np.random.random_integers(- pix_dev, pix_dev, n_pairs)
                y_d += np.random.random_integers(- pix_dev, pix_dev, n_pairs)
                x_o += np.random.random_integers(- pix_dev, pix_dev, n_widows)
                y_o += np.random.random_integers(- pix_dev, pix_dev, n_widows)

            m1 = ((x >= 1) & (x <= self.npix_x)) & ((y >= 1) &
                    (y <= self.npix_y))
            m2 = ((x_d >= 1) & (x_d <= self.npix_x)) & ((y_d >= 1) &
                    (y_d <= self.npix_y))
            m3 = ((x_o >= 1) & (x_o <= self.npix_x)) & ((y_o >= 1) &
                    (y_o <= self.npix_y))

            X = np.concatenate((x[m1], x_d[m2], x_o[m3]))
            Y = np.concatenate((y[m1], y_d[m2], y_o[m3]))

            table = np.column_stack((X, Y))
            np.random.shuffle(table)

            self.actual_nsources += table.shape[0]
            self.camera[i][j] = table

    def get_ccd_images(self, ccds=None):
        """Return the specified CCDs."""
        if ccds is None: return self.camera
        elif not np.isscalar(ccds):
            return tuple([self.camera[iccd] for iccd in ccds])
        else:
            return self.camera[ccds]

    def get_camera_props(self):
        """Return the camera properties in dictionary."""
        n_of_ccd = self.CCD_array_mask.size
        ccd_size = self.npix_x * self.npix_y
        cam_size = ccd_size * n_of_ccd
        props = {
            "number of CCDs": n_of_ccd,
            "CCD size (pix)": ccd_size,
            "camera size (pix)": cam_size
            }
        return props

    def get_image_props(self):
        """Return data properties in dictionary."""
        props = {
            "number of sources": self.actual_nsources,
            "angle (deg)": self.angle,
            "distance (pix)": self.distance
            }
        return props

    def show_image_array(self):
        """Show synthetic images."""

        f, axs = plt.subplots(self.CCD_array_shape[0], self.CCD_array_shape[1],
                                figsize=(10, 10), sharex=True, sharey=True)
        plt.xlim(1, self.npix_x)
        plt.ylim(1, self.npix_y)

        for i, j in product(xrange(self.CCD_array_shape[0]),
                            xrange(self.CCD_array_shape[1])):
            if not self.CCD_array_mask[i, j]:
                continue

            axs[i, j].scatter(self.camera[i][j][:, 0], self.camera[i][j][:, 1],
                                c="k", lw=0, s=9)

        plt.tight_layout()
        plt.subplots_adjust(wspace=0.05, hspace=0.05)
        plt.show()
